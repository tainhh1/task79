package com.devcamp.task79.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.task79.model.Employee;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer> {
	@Query(value = "select *\r\n"
			+ "from employees e \r\n"
			+ "where e.job_title like :jobTitle%", nativeQuery = true)
	List<Object> getEmployeeByJobTitle(@Param("jobTitle") String jobTitle);
}
