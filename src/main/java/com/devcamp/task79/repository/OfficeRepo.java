package com.devcamp.task79.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.task79.model.Office;

@Repository
public interface OfficeRepo extends JpaRepository<Office, Integer> {
	@Query(value = "select *\r\n"
			+ "from offices o \r\n"
			+ "where o.city like %:city%", nativeQuery = true)
	List<Object> findOfficeByCity(@Param("city") String city);
}
