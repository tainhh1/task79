package com.devcamp.task79.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task79.model.ProductLine;

public interface ProductLineRepo extends JpaRepository<ProductLine, Integer> {

}
