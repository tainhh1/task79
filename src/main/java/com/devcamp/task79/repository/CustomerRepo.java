package com.devcamp.task79.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.task79.model.Customer;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, Integer> {
	@Query(value = "select * "
			+ "from customers c "
			+ "where c.first_name like %:firstName% or "
			+ "c.last_name like %:lastName%", nativeQuery = true)
	List<Customer> findCustomerByFirstNameAndLastName(@Param("firstName") String firstName,
			@Param("lastName") String lastName);
	
	@Query(value = "select * "
			+ "from customers c "
			+ "where c.city like %:city% or "
			+ "c.state like %:state%", nativeQuery = true)
	List<Customer> findCustomerByCityAndSate(@Param("city") String city,
			@Param("state") String state, Pageable pageable);
	
	@Query(value = "select *\r\n"
			+ "from customers c \r\n"
			+ "where c.city like %:city%\r\n"
			+ "order by c.last_name ", nativeQuery = true)
	List<Customer> findByCity(@Param("city") String city, Pageable pageable);
	
	@Transactional
	@Modifying
	@Query(value = "update customers c\r\n"
			+ "set c.postal_code = :postalCode \r\n"
			+ "where c.country = null ", nativeQuery = true)
	int updatePostalCode(@Param("postalCode") String postalCode);
}
