package com.devcamp.task79.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.task79.model.Payment;

@Repository
public interface PaymentRepo extends JpaRepository<Payment, Integer> {
	
	@Query(value = "select *\r\n"
			+ "from payments p \r\n"
			+ "where p.customer_id = :customerId", nativeQuery = true)
	List<Object> getAmmount(@Param("customerId") int customerId);
}
