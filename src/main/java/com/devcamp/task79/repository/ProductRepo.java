package com.devcamp.task79.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.task79.model.Product;

@Repository
public interface ProductRepo extends JpaRepository<Product, Integer> {
	@Query(value = "select *\r\n"
			+ "from products p \r\n"
			+ "where p.buy_price > 50", nativeQuery = true)
	List<Object> getProductLarger50(Pageable pageable);
}
