package com.devcamp.task79.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.devcamp.task79.model.OrderDetail;

@Repository
public interface OrderDetailRepo extends JpaRepository<OrderDetail, Integer> {
	@Query(value = "select *\r\n"
			+ "from order_details od \r\n"
			+ "group by od.quantity_order having od.quantity_order > 20", nativeQuery = true)
	List<Object> getQuantityLarger20(Pageable pageable);
}
