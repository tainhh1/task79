package com.devcamp.task79.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.devcamp.task79.model.Order;

@Repository
public interface OrderRepo extends JpaRepository<Order, Integer> {
	@Query(value = "select *\r\n"
			+ "from orders o \r\n"
			+ "where o.customer_id = :customerId", nativeQuery = true)
	List<Object> getOder(@Param("customerId") int customerId);
}
